import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RestServiceService } from './../../shared/service/rest-service.service';
import { StarRatingComponent } from 'ng-starrating';

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.css']
})
export class AddMovieComponent implements OnInit {

  addMovieForm: FormGroup;
  loading = false;
  submitted = false;
  countries;
  genres;
  rating = 0;
  photo: File;

  selectedFile;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private http: RestServiceService) { }

  ngOnInit() {
    this.addMovieForm = this.formBuilder.group({
      name: ['', Validators.required],
      genre: ['', Validators.required],
      description: ['', Validators.required],
      release_date: ['', [Validators.required]],
      // rating: ['', [Validators.required]],
      ticket_price: ['', [Validators.required]],
      country: ['', [Validators.required]],
      photo: ['', [Validators.required]],
    });
    this.http.getDirect('https://restcountries.eu/rest/v2/all').subscribe(data => {
      this.countries = data;
    }, error => {
      console.log('error register', error);
    });
    this.genres = ['Romance', 'Action', 'Western',
      'Thriller', 'Drama', 'Horror', 'Documentary', 'War',
      'Comedy', 'Adventure', 'Animation', 'Family'];
  }

  // convenience getter for easy access to form fields
  get f() { return this.addMovieForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.addMovieForm.invalid) {
      return;
    }
    this.loading = true;
    const payload = new FormData();
    // payload.append('rating', `${this.rating}`);
    payload.append('image', this.selectedFile, this.selectedFile.name);
    this.http.post('films/save-film-image', payload).subscribe(saveImgRes => {
      // this.router.navigate(['/movies']);
      const formData = this.addMovieForm.value;
      formData.rating = this.rating;
      formData.photo = `movie-img/${saveImgRes['fileName']}`;
      this.http.post('films/create', formData).subscribe(saveFilmRes => {
        this.router.navigate([`/films/${saveFilmRes['_id']}`]);
      }, error => {
        console.log('error movie', error);
        this.loading = false;
      });
    }, error => {
      console.log('error movie', error);
      this.loading = false;
    });
  }

  onRate($event: { oldValue: number, newValue: number, starRating: StarRatingComponent }) {
    // alert(`Old Value:${$event.oldValue}, 
    //   New Value: ${$event.newValue}, 
    //   Checked Color: ${$event.starRating.checkedcolor}, 
    //   Unchecked Color: ${$event.starRating.uncheckedcolor}`);
    this.rating = $event.newValue;
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      this.selectedFile = event.target.files[0];
    }
  }

}
