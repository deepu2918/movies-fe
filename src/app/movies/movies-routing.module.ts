import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MoviesComponent } from './movies.component';
import { AddMovieComponent } from './add-movie/add-movie.component';
import { ViewMovieComponent } from './view-movie/view-movie.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: MoviesComponent },
      { path: 'create', component: AddMovieComponent },
      { path: ':id', component: ViewMovieComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MoviesRoutingModule { }
