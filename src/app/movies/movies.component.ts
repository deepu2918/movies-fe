import { Component, OnInit } from '@angular/core';
import { environment } from './../../environments/environment';
import { Router } from '@angular/router';
import { RestServiceService } from './../shared/service/rest-service.service';

export interface Movie {
  id?: number;
  name: string;
  description: string;
  release_date: Date;
  rating: number;
  ticket_price: number;
  country: string;
  genre: [string];
  photo: string;
  status: string;
  comments;
}

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  page = 1;
  pageSize = 1;
  collectionSize;
  movies: Movie[];
  statusCode: number;
  apiUrl: string = environment.apiUrl;

  constructor(private http: RestServiceService, private router: Router) {
    // this.refreshMovies();
  }

  refreshMovies() {
    // this.movies = MOVIES;
    //   .map((country, i) => ({id: i + 1, ...country}))
    //   .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
    this.http.get('films?page=' + this.page).subscribe(data => {
      this.collectionSize = data['data'].count;
      this.movies = data['data'].movies;
    });
  }

  ngOnInit() {
    this.http.get('films').subscribe(data => {
      this.collectionSize = data['data'].count;
      this.movies = data['data'].movies;
    });
  }

}
