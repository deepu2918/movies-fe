import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RatingModule } from 'ng-starrating';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MoviesRoutingModule } from './movies-routing.module';
import { MoviesComponent } from './movies.component';
import { AddMovieComponent } from './add-movie/add-movie.component';
import { ViewMovieComponent } from './view-movie/view-movie.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    MoviesRoutingModule,
    RatingModule,
    ReactiveFormsModule
  ],
  declarations: [MoviesComponent, AddMovieComponent, ViewMovieComponent],
  exports: [MoviesComponent],
  bootstrap: [MoviesComponent]
})
export class MoviesModule { }
