import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RestServiceService } from './../../shared/service/rest-service.service';
import { Movie } from './../movies.component';
import { environment } from './../../../environments/environment';
import { GlobalSubjectService } from './../../shared/service/global-subjects';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-view-movie',
  templateUrl: './view-movie.component.html',
  styleUrls: ['./view-movie.component.css']
})
export class ViewMovieComponent implements OnInit {

  id: string;
  movie: Movie;
  apiUrl: string = environment.apiUrl;
  isLogin = false;
  commentForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  loadMore = true;
  page = 1;

  constructor(private http: RestServiceService,
    private router: ActivatedRoute,
    private subjects: GlobalSubjectService,
    private formBuilder: FormBuilder) { }

  homeDataSubject() {
    this.subjects.HomeData.subscribe(() => {
      if (localStorage.getItem('userName') && localStorage.getItem('userToken')) {
        this.isLogin = true;
      } else {
        this.isLogin = false;
      }
    });
    this.subjects.HomeData.next();
  }

  registerFormValidation() {
    this.commentForm = this.formBuilder.group({
      comment: ['', Validators.required],
    });
  }

  getMovieComments() {
    this.id = this.router.snapshot.paramMap.get('id');
    this.http.get(`films/${this.id}/comments?page=${this.page}`).subscribe(comments => {
      this.movie['comments'].push(...comments['data'].movieComments);
      this.loadMore = comments['data'].loadMore;
    });
  }

  ngOnInit() {
    this.id = this.router.snapshot.paramMap.get('id');
    this.http.get(`films/${this.id}`).subscribe(movies => {
      this.movie = movies['data'].movie;
      this.movie.comments = [];
      this.getMovieComments();
    });
    this.homeDataSubject();
    this.registerFormValidation();
  }

  // convenience getter for easy access to form fields
  get f() { return this.commentForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.commentForm.invalid) {
      return;
    }

    this.loading = true;
    this.http.postAuth(`films/${this.id}/comments/create`, this.commentForm.value).subscribe(data => {
      this.submitted = false;
      this.loading = false;
      this.getMovieComments();
      this.commentForm.reset();
    }, error => {
      alert('Authentication error please login again');
      localStorage.removeItem('userName');
      localStorage.removeItem('userToken');
      this.isLogin = false;
      this.subjects.HomeData.next();
      console.log('error login', error);
      this.loading = false;
    });
  }

  getMore() {
    this.page += 1;
    this.getMovieComments();
  }

}
