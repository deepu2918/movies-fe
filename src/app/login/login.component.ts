import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { RestServiceService } from './../shared/service/rest-service.service';
import { GlobalSubjectService } from './../shared/service/global-subjects';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private http: RestServiceService,
    private subjects: GlobalSubjectService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.http.post('user/login', this.loginForm.value).subscribe(data => {
      console.log('dataaaaaaaaaa login', data);
      localStorage.setItem('userName', data['name']);
      localStorage.setItem('userToken', data['token']);
      this.subjects.HomeData.next();
      this.router.navigate([this.returnUrl]);
    }, error => {
      console.log('error login', error);
      this.loading = false;
    });
  }

}
