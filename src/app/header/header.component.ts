import { Component, OnInit } from '@angular/core';
import { GlobalSubjectService } from './../shared/service/global-subjects';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isLogin = false;
  userName;

  constructor(private subjects: GlobalSubjectService) { }

  ngOnInit() {
    this.subjects.HomeData.subscribe(() => {
      this.userName = localStorage.getItem('userName');
      if (this.userName && localStorage.getItem('userToken')) {
        this.isLogin = true;
      } else {
        this.isLogin = false;
      }
    });
    this.subjects.HomeData.next();
  }

  logout() {
    localStorage.removeItem('userName');
    localStorage.removeItem('userToken');
    this.isLogin = false;
    this.subjects.HomeData.next();
  }
}
