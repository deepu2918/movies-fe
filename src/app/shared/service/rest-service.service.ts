import { HttpClient, HttpHeaders, HttpParams, HttpRequest, } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RestServiceService {
  apiUrl: string = environment.apiUrl;
  constructor(private router: Router, private httpClient: HttpClient,
  ) { }

  post(url, body) {
    return this.httpClient.post(`${this.apiUrl}${url}`, body);
  }

  get(url) {
    return this.httpClient.get(`${this.apiUrl}${url}`);
  }

  getDirect(url) {
    return this.httpClient.get(url);
  }

  postAuth(url, body) {
    const token = localStorage.getItem('userToken');
    const headers = new HttpHeaders({
      Authorization: `${token}`
    });
    return this.httpClient.post(`${this.apiUrl}${url}`, body, { headers });
  }

  postOptions(url, body, options) {
    return this.httpClient.post(`${this.apiUrl}${url}`, body, options);
  }
}
