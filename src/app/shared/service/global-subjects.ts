import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class GlobalSubjectService {
    public RouterData: Subject<any> = new Subject<any>();
    public HomeData: Subject<any> = new Subject<any>();
    // public RouterData: Subject<any> = new Subject<any>();
}